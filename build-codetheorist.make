api = 2
core = 7.x

; Include the definition of how to build Drupal core directly, including patches.
includes[] = "drupal-org-core.make"

; Download the Commons install profile and recursively build all its dependencies.
projects[codetheorist][type] = "profile"
projects[codetheorist][download][type] = "git"
projects[codetheorist][download][url] = "alxs@git.drupal.org:project/codetheorist.git"
projects[codetheorist][download][branch] = "7.x-1.x"
