<?php

/**
 * @file
 * Site csetup & configuration for Codetheorist installation profile.
 */

/**
 * Define commons minimum execution time required to operate.
 */
define('DRUPAL_MINIMUM_MAX_EXECUTION_TIME', 120);

/*
 * Define commons minimum APC cache required to operate.
 */
define('CODETHEORIST_MINIMUM_APC_CACHE', 96);

/**
 * Implements hook_admin_paths_alter().
 */
function codetheorist_admin_paths_alter(&$paths) {
  // Avoid switching between themes when users edit their account.
  $paths['user'] = FALSE;
  $paths['user/*'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function codetheorist_form_install_configure_form_alter(&$form, $form_state) {
  // Clear all non-error messages that might be set by enabled modules
  drupal_get_messages('status', TRUE);
  drupal_get_messages('completed', TRUE);

  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];

  $form['admin_account']['field_name_first'] = array(
    '#type' => 'textfield',
    '#title' => 'First name',
    '#weight' => -10,
  );

  $form['admin_account']['field_name_last'] = array(
    '#type' => 'textfield',
    '#title' => 'Last name',
    '#weight' => -9,
  );

  $form['#submit'][] = 'codetheorist_admin_save_fullname';

}

/**
 * Implements hook_update_projects_alter().
 */
function codetheorist_update_projects_alter(&$projects) {
  // Enable update status for the Codetheorist profile.
  $modules = system_rebuild_module_data();
  // The module object is shared in the request, so we need to clone it here.
  $codetheorist = clone $modules['codetheorist'];
  $codetheorist->info['hidden'] = FALSE;
  _update_process_info_list($projects, array('codetheorist' => $codetheorist), 'module', TRUE);
}

/**
 * Save the full name of the first user.
 */
function codetheorist_admin_save_fullname($form_id, &$form_state) {
  $values = $form_state['values'];
  if (!empty($values['field_name_first']) || !empty($values['field_name_last'])) {
    $account = user_load(1);
    $account->field_name_first[LANGUAGE_NONE][0]['value'] = $values['field_name_first'];
    $account->field_name_last[LANGUAGE_NONE][0]['value'] = $values['field_name_last'];
    user_save($account);
    realname_update($account);
  }
}
