api = 2
core = 7.x

; Contributed modules.

projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc5"

projects[adminimal_admin_menu][type] = "module"
projects[adminimal_admin_menu][subdir] = "contrib"
projects[adminimal_admin_menu][version] = "1.7"

projects[advanced_help][type] = "module"
projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.3"

projects[ckeditor][type] = "module"
projects[ckeditor][subdir] = "contrib"
projects[ckeditor][version] = "1.17"

; Fix features rebuilding when CKEditor is disabled.
; https://www.drupal.org/node/2456519#comment-9742435
projects[ckeditor][patch][] = "https://drupal.org/files/issues/ckeditor-ckeditor-feature-2456519-2.patch"

projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.12"

projects[date][type] = "module"
projects[date][subdir] = "contrib"
projects[date][version] = "2.9"

; Keeping this to the latest version, since it should only be used for development.
projects[devel][version] = "1.x-dev"
projects[devel][type] = "module"
projects[devel][subdir] = "contrib"

projects[diff][type] = "module"
projects[diff][subdir] = "contrib"
projects[diff][version] = "3.3"

; Profile has no recommended release.
projects[edit_profile][type] = "module"
projects[edit_profile][subdir] = "contrib"
projects[edit_profile][version] = "1.0-beta2"

projects[email_registration][type] = "module"
projects[email_registration][subdir] = "contrib"
projects[email_registration][version] = "1.3"

projects[entity][type] = "module"
projects[entity][subdir] = "contrib"
projects[entity][version] = "1.8"

projects[entitycache][type] = "module"
projects[entitycache][subdir] = "contrib"
projects[entitycache][version] = "1.5"

projects[features][type] = "module"
projects[features][subdir] = "contrib"
projects[features][version] = "2.10"

projects[file_entity][type] = "module"
projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.0-beta3"

projects[flag][type] = "module"
projects[flag][subdir] = "contrib"
projects[flag][version] = "3.9"

projects[flag_abuse][type] = "module"
projects[flag_abuse][subdir] = "contrib"
projects[flag_abuse][version] = "2.0"

projects[gravatar][type] = "module"
projects[gravatar][subdir] = "contrib"
projects[gravatar][download][type] = "git"
projects[gravatar][download][url] = "http://git.drupal.org/project/gravatar.git"
projects[gravatar][download][branch] = "7.x-1.x"
projects[gravatar][download][revision] = "bb2f81e6"

projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.7"

projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.3"

projects[link][type] = "module"
projects[link][subdir] = "contrib"
projects[link][version] = "1.4"

projects[media][type] = "module"
projects[media][subdir] = "contrib"
projects[media][version] = "2.0-rc12"

projects[media_soundcloud][type] = "module"
projects[media_soundcloud][subdir] = "contrib"
projects[media_soundcloud][version] = "2.1"

projects[media_vimeo][type] = "module"
projects[media_vimeo][subdir] = "contrib"
projects[media_vimeo][version] = "2.1"

projects[media_youtube][type] = "module"
projects[media_youtube][subdir] = "contrib"
projects[media_youtube][version] = "3.0"

projects[menu_attributes][type] = "module"
projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][version] = "1.0"

projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.0"

projects[multiform][type] = "module"
projects[multiform][subdir] = "contrib"
projects[multiform][version] = "1.3"

projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"

projects[plup][type] = "module"
projects[plup][subdir] = "contrib"
projects[plup][version] = "1.0-alpha1"

projects[plupload][type] = "module"
projects[plupload][subdir] = "contrib"
projects[plupload][version] = "1.7"

projects[r4032login][type] = "module"
projects[r4032login][subdir] = "contrib"
projects[r4032login][version] = "1.8"

projects[realname][type] = "module"
projects[realname][subdir] = "contrib"
projects[realname][version] = "1.3"

; Realname entityreference autocomplete API update
; https://drupal.org/node/2225889
projects[realname][patch][] = "https://drupal.org/files/issues/2225889-realname-correct-menu-3.patch"

projects[redirect][type] = "module"
projects[redirect][subdir] = "contrib"
projects[redirect][version] = "1.0-rc3"

projects[registration][subdir] = "contrib"
projects[registration][type] = "module"
projects[registration][version] = "1.6"

projects[rich_snippets][type] = "module"
projects[rich_snippets][subdir] = "contrib"
projects[rich_snippets][version] = "1.0-beta4"

projects[schemaorg][type] = "module"
projects[schemaorg][subdir] = "contrib"
projects[schemaorg][version] = "1.0-rc1"

projects[services][type] = "module"
projects[services][subdir] = "contrib"
projects[services][version] = "3.19"

projects[smartcrop][type] = "module"
projects[smartcrop][subdir] = "contrib"
projects[smartcrop][version] = "1.0-beta2"

projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[token][version] = "1.7"

projects[variable][type] = "module"
projects[variable][subdir] = "contrib"
projects[variable][version] = "2.5"

projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views][version] = "3.15"

; Update Views Content access filter per core performance improvements.
; https://drupal.org/comment/8516039#comment-8516039
projects[views][patch][] = "https://drupal.org/files/issues/views-content_access_filter_per_core_performance-2204257-4_0.patch"

projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.4"

projects[webform][type] = "module"
projects[webform][subdir] = "contrib"
projects[webform][version] = "4.14"

; Contributed themes.

projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][subdir] = "contrib"
projects[adminimal_theme][version] = "1.24"

; Error if module_filter not enabled.
; https://www.drupal.org/node/2763581#comment-11824900
projects[adminimal_theme][patch][] = "https://www.drupal.org/files/issues/adminimal_theme_1_24-2763581-34_0.patch"

projects[zurb_foundation][type] = "theme"
projects[zurb_foundation][subdir] = "contrib"
projects[zurb_foundation][version] = "5.0-rc6"

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.7/ckeditor_4.4.7_full.zip"
libraries[ckeditor][type] = "libraries"

libraries[ckeditor_lineutils][download][type] = "get"
libraries[ckeditor_lineutils][download][url] = "http://download.ckeditor.com/lineutils/releases/lineutils_4.4.7.zip"
libraries[ckeditor_lineutils][type] = "libraries"
libraries[ckeditor_lineutils][subdir] = "ckeditor/plugins"
libraries[ckeditor_lineutils][directory_name] = "lineutils"

libraries[ckeditor_widget][download][type] = "get"
libraries[ckeditor_widget][download][url] = "http://download.ckeditor.com/widget/releases/widget_4.4.7.zip"
libraries[ckeditor_widget][type] = "libraries"
libraries[ckeditor_widget][subdir] = "ckeditor/plugins"
libraries[ckeditor_widget][directory_name] = "widget"

libraries[modernizr][download][type] = "get"
libraries[modernizr][type] = "libraries"
libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/v2.7.1.tar.gz"

libraries[underscore][download][type] = "get"
libraries[underscore][type] = "libraries"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/1.5.2.zip"
